export interface AppState {
    restaurants: Restaurant[];
    location: LatLong | null;
    activeFilter: string;
    currentSearch: string;
    parameters: Parameters;
    showCredits: boolean;
}

export interface Parameters {
    filters: Filters;
    sorts: Sorts;
}

export interface Filters {
    type?: string;
    neighborhood?: string;
    priceRange?: string;
    isNew?: boolean;
    businessType?: string;
    isBest?: boolean;
    search?: string;
}

export interface Sorts {
    location?: boolean;
}

export interface LatLong {
    latitude: number;
    longitude: number;
}

export interface Restaurant {
    name: string;
    review?: string;
    address: string;
    neighborhood: string;
    type: string;
    filterType: string;
    phone: string;
    latitude: number;
    longitude: number;
    distance?: number;
    image: string;
    priceRange: string;
    isNew?: boolean;
    is_new?: string;
    is_best?: string;
    isBest?: boolean;
    businessType?: string;
}

export interface ControlledInput {
    value: string;
    onChange?: Function;
    name?: string;
    placeholder?: string;
}

export interface Button {
    text?: string;
    onClick?: Function;
}

export interface Credits {
    show: boolean;
}

export interface ActionDropdown {
    name: string;
    options: string[];
    onChange?: Function;
    onChooseDefault?: Function;
}

export interface ControlledCheckbox {
    text: string;
    checked?: boolean;
    onCheck: Function;
    onUncheck: Function;
}