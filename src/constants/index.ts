export const SET_SEARCH_TERM = 'SET_SEARCH_TERM';
export type SET_SEARCH_TERM = typeof SET_SEARCH_TERM;

export const SET_LOCATION = 'SET_LOCATION';
export type SET_LOCATION = typeof SET_LOCATION;

export const TOGGLE_CREDITS = 'TOGGLE_CREDITS';
export type TOGGLE_CREDITS = typeof TOGGLE_CREDITS;

export const SET_FILTER = 'SET_FILTER';
export type SET_FILTER = typeof SET_FILTER;

export const REMOVE_FILTER = 'REMOVE_FILTER';
export type REMOVE_FILTER = typeof REMOVE_FILTER;

export const RESET_ALL_FILTERS = 'RESET_ALL_FILTERS';
export type RESET_ALL_FILTERS = typeof RESET_ALL_FILTERS;

export const RESET_ALL_SORTS = 'RESET_ALL_SORTS';
export type RESET_ALL_SORTS = typeof RESET_ALL_SORTS;