import * as constants from '../constants';
import { LatLong } from '../types';

export interface Action {
    type: string;
}

export interface SetSearchTerm extends Action {
    type: constants.SET_SEARCH_TERM;
    term: string;
}

export interface SetLocation extends Action {
    type: constants.SET_LOCATION;
    location: LatLong;
}

export interface ToggleCredits extends Action {
    type: constants.TOGGLE_CREDITS;
}

export interface SetFilter extends Action {
    type: constants.SET_FILTER;
    filterName: string;
    value: string | boolean;
}

export interface RemoveFilter extends Action {
    type: constants.REMOVE_FILTER;
    filterName: string;
}

export interface ResetAllFilters extends Action {
    type: constants.RESET_ALL_FILTERS;
}

export interface ResetAllSorts extends Action {
    type: constants.RESET_ALL_SORTS;
}

export const setSearchTerm = (term: string): SetSearchTerm => {
    return {
        type: constants.SET_SEARCH_TERM,
        term
    };
};

export const setLocation = (location: LatLong): SetLocation => {
    return {
        type: constants.SET_LOCATION,
        location
    };
};

export const toggleCredits = (): ToggleCredits => {
    return {
        type: constants.TOGGLE_CREDITS
    };
};

export const setFilter = (filterName: string, value: string | boolean): SetFilter => {
    return {
        type: constants.SET_FILTER,
        filterName,
        value
    };
};

export const removeFilter = (filterName: string): RemoveFilter => {
    return {
        type: constants.REMOVE_FILTER,
        filterName
    };
};

export const resetAllFilters = (): ResetAllFilters => {
    return {
        type: constants.RESET_ALL_FILTERS
    };
};

export const resetAllSorts = (): ResetAllSorts => {
    return {
        type: constants.RESET_ALL_SORTS
    };
};