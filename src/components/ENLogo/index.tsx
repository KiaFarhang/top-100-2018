import * as React from 'react';

import './ENLogo.css';

export default () => (
	<div className="ENLogo">
		<a href="https://www.expressnews.com/" target="_blank">
			<img
				src="https://s3.amazonaws.com/projects.expressnews.com/cadets/chapter-one/static/media/en_logo.8b6956a8.svg"
				alt="Express News"
			/>
		</a>
	</div>
);