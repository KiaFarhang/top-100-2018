import * as React from 'react';

export default () => (
	<div className="TwitterLogo">
		<img
			src="https://s3.amazonaws.com/projects.expressnews.com/cadets/chapter-one/static/media/twitter_logo.c849aa7f.svg"
			alt="Twitter"
		/>
	</div>
);