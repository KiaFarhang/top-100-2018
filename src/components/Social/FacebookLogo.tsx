import * as React from 'react';

export default () => (
	<div className="FacebookLogo">
		<img
			src="https://s3.amazonaws.com/projects.expressnews.com/cadets/chapter-one/static/media/fb_logo.0221e2fd.svg"
			alt="Facebook"
		/>
	</div>
);