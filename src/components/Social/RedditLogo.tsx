import * as React from 'react';

export default () => (
	<div className="RedditLogo">
		<img
			src="https://s3.amazonaws.com/projects.expressnews.com/cadets/chapter-one/static/media/reddit_logo.63def9bd.svg"
			alt="Reddit"
		/>
	</div>
);