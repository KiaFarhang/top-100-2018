import { connect } from 'react-redux';
import Filters from '../Filters';
import { AppState } from '../../types';

const mapStateToProps = (state: AppState) => {
    return {
        businesses: state.restaurants
    };
};

export default connect(
    mapStateToProps
)(Filters);