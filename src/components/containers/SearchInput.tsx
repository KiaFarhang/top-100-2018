import { connect, Dispatch } from 'react-redux';
import ControlledInput from '../ControlledInput';
import { AppState } from '../../types';
import { setSearchTerm, SetSearchTerm, setFilter, SetFilter } from '../../actions';
import { ChangeEvent } from 'react';

export interface Props {
    placeholder: string;
}

const mapStateToProps = (state: AppState, { placeholder }: Props) => {
    const searchState = state.parameters.filters.search;
    return {
        value: searchState ? searchState : '',
        placeholder
    };
};

const mapDispatchToProps = (dispatch: Dispatch<SetSearchTerm>) => {
    return {
        onChange: (event: ChangeEvent<HTMLInputElement>) => dispatch(setFilter('search', event.target.value))
    };
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(ControlledInput);