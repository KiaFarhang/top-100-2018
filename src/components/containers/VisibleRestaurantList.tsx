import { connect } from 'react-redux';
import RestaurantList from '../RestaurantList';
import * as types from '../../types';

import { getGreatCircleDistanceInKM } from '../../util';
import { LatLong } from '../../types';

const getVisibleRestaurants = (restaurants: types.Restaurant[], currentSearch: string,
    location: types.LatLong | null): types.Restaurant[] => {

    if (currentSearch === '' && !location) {
        return restaurants;
    } else {

        const filterBySearch = (restaurants: types.Restaurant[]): types.Restaurant[] => {
            return restaurants.filter(restaurant =>
                restaurant.name.toLowerCase().includes(currentSearch.toLowerCase()));
        };

        const filterByLocation = (restaurants: types.Restaurant[]): types.Restaurant[] => {
            // First, we create a new restaurant array with distance to current location calculated.

            const restaurantsWithDistancesCalculated = restaurants.map((restaurant) => {
                const restaurantLatLong: types.LatLong = {
                    latitude: restaurant.latitude,
                    longitude: restaurant.longitude
                };
                return Object.assign({}, restaurant,
                    { distance: getGreatCircleDistanceInKM(location as LatLong, restaurantLatLong) });
            });

            // Then, we sort that array on distance to current location and return it.

            return restaurantsWithDistancesCalculated.sort((a: types.Restaurant, b: types.Restaurant): number => {
                return (a.distance as number) - (b.distance as number);
            });
        };

        let restaurantsToReturn = restaurants;

        if (currentSearch !== '') { restaurantsToReturn = filterBySearch(restaurantsToReturn); }

        if (location !== null) { restaurantsToReturn = filterByLocation(restaurantsToReturn); }

        return restaurantsToReturn;

    }
};

const filterBusinesses = (businesses: types.Restaurant[],
    parameters: types.Parameters,
    location: types.LatLong | null): types.Restaurant[] => {
    let listToReturn: types.Restaurant[] = businesses;
    Object.keys(parameters.filters).forEach((key) => {
        // if the key is neighborhood, type or price range, we want any business
        // with a property that includes said key

        if (key === 'neighborhood' || key === 'type' || key === 'businessType') {
            listToReturn = listToReturn.filter((business: types.Restaurant) => {
                const businessValue = business[key] as string;
                const filterValue = parameters.filters[key] as string;
                return businessValue.toLowerCase().includes(filterValue.toLowerCase());
            });
        }

        if (key === 'priceRange') {
            listToReturn = listToReturn.filter((business: types.Restaurant) => {
                const businessValue = business[key] as string;
                const filterValue = parameters.filters[key] as string;
                return filterValue === businessValue;
            });
        }

        // if the key is isNew, we want an exact match on the boolean property

        if (key === 'isNew' || key === 'isBest') {
            listToReturn = listToReturn.filter((business: types.Restaurant) => {
                return business[key] === parameters.filters[key];
            });
        }
    });

    if (parameters.filters.search && parameters.filters.search !== '') {
        const { search } = parameters.filters;
        listToReturn = listToReturn.filter((business: types.Restaurant) => {
            return business.neighborhood.toLowerCase().includes(search.toLowerCase())
                || business.type.toLowerCase().includes(search.toLowerCase())
                || business.name.toLowerCase().includes(search.toLowerCase());
        });
    }

    // Sort on location if necessary

    if (parameters.sorts.location && location !== null) {
        const sortByLocation = (restaurants: types.Restaurant[], location: types.LatLong): types.Restaurant[] => {
            // First, we create a new restaurant array with distance to current location calculated.

            const restaurantsWithDistancesCalculated = restaurants.map((restaurant) => {
                const restaurantLatLong: types.LatLong = {
                    latitude: restaurant.latitude,
                    longitude: restaurant.longitude
                };
                return Object.assign({}, restaurant,
                    { distance: getGreatCircleDistanceInKM(location as LatLong, restaurantLatLong) });
            });

            // Then, we sort that array on distance to current location and return it.

            return restaurantsWithDistancesCalculated.sort((a: types.Restaurant, b: types.Restaurant): number => {
                return (a.distance as number) - (b.distance as number);
            });
        };

        listToReturn = sortByLocation(listToReturn, location);
    }

    return listToReturn;

};

const mapStateToProps = (state: types.AppState) => ({
    // restaurants: getVisibleRestaurants(state.restaurants, state.currentSearch, state.location)
    restaurants: filterBusinesses(state.restaurants, state.parameters, state.location)
});

const VisibleRestaurantList = connect(
    mapStateToProps
)(RestaurantList);

export default VisibleRestaurantList;