import { connect, Dispatch } from 'react-redux';
import ActionDropdown from '../ActionDropdown';
import { AppState } from '../../types';
import { setFilter, SetFilter, removeFilter, RemoveFilter } from '../../actions';

interface Props {
    name: string;
    options: string[];
    propertyToUpdate: string;
}

const mapDispatchToProps = (dispatch: Dispatch<SetFilter | RemoveFilter>,
    { name, options, propertyToUpdate }: Props) => {
    return {
        onChange: (value: string) => {
            dispatch(setFilter(propertyToUpdate, value));
        },
        onChooseDefault: () => {
            dispatch(removeFilter(propertyToUpdate));
        },
        name,
        options
    };
};

export default connect(
    null,
    mapDispatchToProps
)(ActionDropdown);
