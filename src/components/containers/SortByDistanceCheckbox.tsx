import { connect, Dispatch } from 'react-redux';
import ControlledCheckbox from '../ControlledCheckbox';
import { AppState } from '../../types';
import { setLocation, SetLocation, resetAllSorts, ResetAllSorts } from '../../actions';

interface Props {
    text: string;
}

const mapStateToProps = (state: AppState, { text }: Props) => {
    return {
        checked: state.parameters.sorts.location ? true : false,
        text
    };
};

const mapDispatchToProps = (dispatch: Dispatch<SetLocation | ResetAllSorts>) => {
    return {
        onCheck: () => {
            if ('geolocation' in navigator) {
                navigator.geolocation.getCurrentPosition((position) => {
                    const { latitude, longitude } = position.coords;
                    dispatch(setLocation({ latitude, longitude }));
                });
            }
        },
        onUncheck: () => {
            dispatch(resetAllSorts());
        }
    };
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(ControlledCheckbox);