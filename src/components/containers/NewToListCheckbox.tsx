import { connect, Dispatch } from 'react-redux';
import ControlledCheckbox from '../ControlledCheckbox';
import { AppState } from '../../types';
import { setFilter, SetFilter, removeFilter, RemoveFilter } from '../../actions';

interface Props {
    text: string;
}

const mapStateToProps = (state: AppState, { text }: Props) => {
    return {
        checked: state.parameters.filters.isNew,
        text
    };
};

const mapDispatchToProps = (dispatch: Dispatch<SetFilter | RemoveFilter>) => {
    return {
        onCheck: () => {
            dispatch(setFilter('isNew', true));
        },
        onUncheck: () => {
            dispatch(removeFilter('isNew'));
        }
    };
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(ControlledCheckbox);