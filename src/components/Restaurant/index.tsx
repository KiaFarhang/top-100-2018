import * as React from 'react';
import { Restaurant as Props } from '../../types';

import { precisionRound } from '../../util';

import './Restaurant.css';

const Restaurant = ({ name, review, address, neighborhood,
    phone, isBest, image, distance, priceRange, type, businessType }: Props) => {

    const imageHTML = review ? (
        <a href={review} title={name}>
            <img src={image.replace('920x920', '500x500')} alt={name} />
        </a>
    ) : (
            <img src={image.replace('920x920', '500x500')} alt={name} />
        );

    const reviewLink = !review ? null : (
        <p> <a href={review} title={name}>Why it's on the list</a></p>
    );

    return (
        <div className="Restaurant">
            {imageHTML}
            <div className="info">
                <h4>{isBest && businessType !== 'Restaurant' ? `Best: ${name}` : `${name}`}</h4>
                <p className="type-neighborhood">{type} {neighborhood ? `| ${neighborhood}` : null}</p>
                <p className="price">{priceRange}</p>
                {reviewLink}
            </div>
        </div>
    );
};

export default Restaurant;