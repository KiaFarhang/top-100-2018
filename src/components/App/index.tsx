import * as React from 'react';
import AppHeader from '../AppHeader';
import NavBar from '../NavBar';
import ENLogo from '../ENLogo';
import SocialBlock from '../SocialBlock';

import VisibleRestaurantList from '../containers/VisibleRestaurantList';

import './App.css';

const App = () => {
  return (
    <div className="App">
      <NavBar>
        <div className="left">
          <ENLogo />
        </div>
        <div className="right">
          <SocialBlock url="https://www.expressnews.com/best-san-antonio-bars-restaurants-2018/" />
        </div>
      </NavBar>
      <AppHeader />
      <VisibleRestaurantList />
    </div>
  );
};

export default App;
