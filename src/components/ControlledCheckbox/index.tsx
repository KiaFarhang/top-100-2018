import * as React from 'react';
import { ControlledCheckbox as Props } from '../../types';

interface State {
    checked: boolean;
}

class ControlledCheckbox extends React.Component<Props, State> {
    constructor(props: Props) {
        super(props);
        this.state = {
            checked: props.checked ? true : false
        };
    }

    handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        const checkbox = event.target as HTMLInputElement;
        if (checkbox.checked) {
            this.props.onCheck();
            this.setState(({
                checked: true
            }));
        } else {
            this.props.onUncheck();
            this.setState(({
                checked: false
            }));
        }
    }

    render() {
        return (
            <div className="checkbox">
                <input
                    type="checkbox"
                    checked={this.state.checked}
                    onChange={this.handleChange}
                    value={this.props.text}
                    id={this.props.text}
                />
                <label htmlFor={this.props.text}>{this.props.text}</label>
            </div>
        );
    }
}
// tslint:disable-next-line
export default ControlledCheckbox as any;