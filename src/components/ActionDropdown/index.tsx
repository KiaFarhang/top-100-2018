import * as React from 'react';
import { ActionDropdown as Props } from '../../types';

const ActionDropdown = ({ name, options, onChange, onChooseDefault }: Props) => {
    const optionItems = options.map((option: string, index: number) => {
        return <option value={option} key={index}>{option}</option>;
    });

    const handleChange = (event: React.ChangeEvent<HTMLSelectElement>) => {
        const select = event.nativeEvent.target as HTMLSelectElement;
        const selectedIndex = select.selectedIndex;
        if (selectedIndex > 0 && onChange) {
            onChange(event.target.value);
        } else if (selectedIndex === 0 && onChooseDefault) {
            onChooseDefault();
        }
    };

    return (
        <select onChange={handleChange}>
            <option value={name}>{name}</option>
            {optionItems}
        </select >
    );
};

export default ActionDropdown;