import * as React from 'react';
import ShowCreditsButton from '../containers/ShowCreditsButton';
import VisibleCredits from '../containers/VisibleCredits';
import FilterContainer from '../containers/FilterContainer';

import './AppHeader.css';

const AppHeader = () => {
    const showNearMe = window.location.protocol.includes('https:') && navigator.geolocation && !isFacebookApp();
    return (
        <div className="AppHeader">
            <img
                // tslint:disable-next-line
                src="https://s3.amazonaws.com/projects.expressnews.com/top-100-2018/static/media/header.216d5cb8.svg"
                alt="Express-News Top 100"
            />
            <div className="chatter">
                <p>If you think sorting through all of San Antonio’s outstanding restaurants, bars, bakeries,
                    distilleries, breweries, wineries and coffee shops to proclaim the best is tough, you’re right.
                Delicious, of course, but definitely hard. (Of course, we love a challenge.)</p>
                <p>Search or filter to find the top spots for the right price in your part of town.</p>
            </div>
            <FilterContainer />
            <div>
                <ShowCreditsButton text="Credits" />
                <VisibleCredits />
            </div>
        </div>
    );
};

function isFacebookApp() {
    const ua = navigator.userAgent || navigator.vendor;
    return (ua.indexOf('FBAN') > -1) || (ua.indexOf('FBAV') > -1);
}

export default AppHeader;