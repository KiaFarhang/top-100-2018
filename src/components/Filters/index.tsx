import * as React from 'react';
import { Restaurant } from '../../types';
import FilterDropdown from '../containers/FilterDropdown';
import NewToListCheckbox from '../containers/NewToListCheckbox';
import SortByDistanceCheckbox from '../containers/SortByDistanceCheckbox';
import BestCheckbox from '../containers/BestCheckbox';
import SearchInput from '../containers/SearchInput';

import './Filters.css';

interface Props {
    businesses: Restaurant[];
}

const Filters = ({ businesses }: Props) => {

    // We have to do some massaging to extract a list of neighborhoods
    // Some businesses have a comma+space separated list of them rather
    // than just one.

    let neighborhoods: string[] = [];

    businesses.forEach((business) => {
        const { neighborhood } = business;
        if (!neighborhood.includes(',')) {
            neighborhoods.push(neighborhood);
        } else {
            const splits = neighborhood.split(',');
            splits.forEach((str) => {
                neighborhoods.push(str.trim());
            });
        }
    });

    const types = businesses.map((business) => {
        return business.filterType;
    });

    const uniqueNeighborhoods = Array.from(new Set(neighborhoods)).filter((neighborhood) => neighborhood !== '')
        .sort((neighborhoodA, neighborhoodB) => {
            return neighborhoodA.localeCompare(neighborhoodB);
        });
    const uniqueTypes = Array.from(new Set(types)).filter((type: string) => {
        const ignoredTypes = ['Bar', 'Winery', 'Brewery', 'Distillery', 'Bakery', 'Coffee Shop'];
        return ignoredTypes.indexOf(type) < 0;
    });

    const businessTypes = [
        'Restaurant', 'Bar', 'Winery', 'Brewery', 'Distillery', 'Bakery', 'Coffee Shop',
    ];

    return (
        <div className="filters">
            <div className="dropdowns">
                <FilterDropdown name="Neighborhood" options={uniqueNeighborhoods} propertyToUpdate={'neighborhood'} />
                <FilterDropdown name="Category" options={businessTypes} propertyToUpdate={'businessType'} />
                <FilterDropdown name="Cuisine" options={uniqueTypes} propertyToUpdate={'type'} />
                <FilterDropdown
                    name="Price range"
                    options={['$', '$$', '$$$', '$$$$']}
                    propertyToUpdate={'priceRange'}
                />
            </div>
            <NewToListCheckbox text={'New to the list'} />
            <BestCheckbox text={'Best of the best'} />
            <SortByDistanceCheckbox text={'Sort by distance to me'} />
            <SearchInput placeholder="Search by name, neighborhood or cuisine" />
        </div>
    );
};

export default Filters;