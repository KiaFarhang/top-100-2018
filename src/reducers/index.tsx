import { omit } from 'lodash';
import { combineReducers } from 'redux';

import * as types from '../types';
import * as constants from '../constants';
import * as actions from '../actions';

const businesses: types.Restaurant[] = require('../top-100-updates.json');

// The data comes through from Google with "TRUE" / "FALSE"
// values for the isNew property, so we have to transform
// them to actual booleans.

let businessesWithBooleans = businesses.map((business) => {
    if (business.is_new && business.is_new.toLowerCase() === 'true') {
        return Object.assign({}, business, { isNew: true });
    } else if (business.is_new && business.is_new.toLowerCase() === 'false') {
        return Object.assign({}, business, { isNew: false });
    }
    return business;
});

// Then do the same transformation on the isBest property.

businessesWithBooleans = businessesWithBooleans.map((business) => {
    if (business.is_best && business.is_best.toLowerCase() === 'true') {
        return Object.assign({}, business, { isBest: true });
    } else if (business.is_best && business.is_best.toLowerCase() === 'false') {
        return Object.assign({}, business, { isBest: false });
    }
    return business;
});

export const initialState: types.AppState = {
    restaurants: businessesWithBooleans,
    location: null,
    parameters: {
        filters: {},
        sorts: {}
    },
    activeFilter: 'SHOW_ALL',
    currentSearch: '',
    showCredits: false
};

export const currentSearch = (action: actions.SetSearchTerm): string => {
    if (action.term) {
        return action.term;
    }
    return '';
};

export const location = (state: types.AppState, action: actions.SetLocation): types.AppState => {
    const { parameters } = state;
    const newParameters = Object.assign({}, parameters, { sorts: { location: true } });
    return Object.assign({}, state, { parameters: newParameters }, { location: action.location });
};

export const credits = (showCredits: boolean): boolean => {
    return !showCredits;
};

export const filters = (state: types.Filters,
    action: actions.SetFilter | actions.RemoveFilter | actions.ResetAllFilters): types.Filters => {
    switch (action.type) {
        case constants.SET_FILTER:
            return Object.assign({}, state, { [action.filterName]: action.value });
        case constants.REMOVE_FILTER:
            return omit(state, [action.filterName]);
        case constants.RESET_ALL_FILTERS:
            return {};
        default: return state;
    }
};

export const sorts = (state: types.Sorts, action: actions.ResetAllSorts): types.Sorts => {
    switch (action.type) {
        case constants.RESET_ALL_SORTS:
            return {};
        default: return state;
    }
};

export const parameters = (state: types.Parameters,
    action: actions.SetFilter | actions.RemoveFilter |
        actions.ResetAllFilters | actions.ResetAllSorts): types.Parameters => {
    switch (action.type) {
        case constants.SET_FILTER:
        case constants.REMOVE_FILTER:
        case constants.RESET_ALL_FILTERS:
            return Object.assign({}, state, { filters: filters(state.filters, action) });
        case constants.RESET_ALL_SORTS:
            return Object.assign({}, state, { sorts: sorts(state.sorts, action) });
        default: return state;
    }
};

export const tacoApp = (state: types.AppState = initialState, action: actions.Action): types.AppState => {
    switch (action.type) {
        case constants.SET_SEARCH_TERM:
            return Object.assign({}, state, { currentSearch: currentSearch(action as actions.SetSearchTerm) });
        case constants.SET_LOCATION:
            return Object.assign({}, state, location(state, action as actions.SetLocation));
        case constants.TOGGLE_CREDITS:
            return Object.assign({}, state, { showCredits: credits(state.showCredits) });
        case constants.SET_FILTER:
        case constants.REMOVE_FILTER:
        case constants.RESET_ALL_FILTERS:
            return Object.assign({}, state, {
                parameters: parameters(state.parameters,
                    action as actions.SetFilter | actions.RemoveFilter | actions.ResetAllFilters)
            });
        case constants.RESET_ALL_SORTS:
            return Object.assign({}, state,
                { parameters: parameters(state.parameters, action as actions.ResetAllSorts) });
        default:
            return state;
    }
};