import * as reducers from './index';
import * as actions from '../actions';

describe('Filter reducer', () => {
    const reducer = reducers.filters;
    describe('SetFilter action', () => {
        it('Adds the given filter to state if not already assigned', () => {
            const state = {};
            const action = actions.setFilter('neighborhood', 'Downtown');
            const newState = reducer(state, action);
            expect(newState).toMatchObject({
                neighborhood: 'Downtown'
            });
        });
        it('Updates the given filter to new value if it already exists', () => {
            const state = {
                neighborhood: 'Downtown'
            };
            const action = actions.setFilter('neighborhood', 'Uptown');
            const newState = reducer(state, action);
            expect(newState).toMatchObject({
                neighborhood: 'Uptown'
            });
        });
        it('Does not modify filters other than the one in the action passed', () => {
            const state = {
                neighborhood: 'Downtown'
            };
            const action = actions.setFilter('priceRange', '$$');
            const newState = reducer(state, action);
            expect(newState).toMatchObject({
                neighborhood: 'Downtown',
                priceRange: '$$'
            });
        });
    });
    describe('removeFilter action', () => {
        it('removes the given property from state object if it exists', () => {
            const state = {
                neighborhood: 'Downtown'
            };
            const action = actions.removeFilter('neighborhood');
            const newState = reducer(state, action);
            expect(newState).toMatchObject({});
        });
        it('does not touch other properties on the state object', () => {
            const state = {
                neighborhood: 'Downtown',
                priceRange: '$$'
            };
            const action = actions.removeFilter('neighborhood');
            const newState = reducer(state, action);
            expect(newState).toMatchObject({
                priceRange: '$$'
            });
        });
        it('does not modify the state object if it does not contain the given property', () => {
            const state = {
                neighborhood: 'Downtown'
            };
            const action = actions.removeFilter('priceRange');
            const newState = reducer(state, action);
            expect(newState).toMatchObject(state);
        });
    });
    describe('resetAllFilters action', () => {
        it('returns an empty object', () => {
            const state = {
                neighborhood: 'Downtown'
            };
            const action = actions.resetAllFilters();
            const newState = reducer(state, action);
            expect(newState).toMatchObject({});
        });
    });
});
