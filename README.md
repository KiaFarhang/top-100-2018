This repository holds the React + Redux + TypeScript code for [the Express-News Top 100 2018 page.](https://www.expressnews.com/best-san-antonio-bars-restaurants-2018/)

To run the project locally, clone it and install dependencies with `npm install` or `yarn`. Then run the start script with `npm run start` or `yarn start`.